import React, { Component } from 'react';
import { 
        StyleSheet, 
        View,
        Image,
         NetInfo
         } from 'react-native';
import {
         // BackgroundImage,
         InputForm,
         InputSection,
         Input,
         Spinner,
         FormMessage,
         Button
       } from './components/common';
import SharedStyles from './SharedStyles';
import StyleVars from './StyleVars';
import bgImage from './images/logo-final.png';
import { loginUser } from './actions/LoginActions';
import Constants from './Constants';
//import { connectionShape } from 'react-native-connection-info';

export default class Login extends Component {

 constructor() {
    super();
    this.state = {
      //isConnected: false,
    };
    //this.handleIsConnected = this.handleIsConnected.bind(this);
  }

  componentWillMount() {
    this.setState({
      username: '',
      password: '', 
      loading: false,
      info: '',
      error: ''
    });
    

//  NetInfo.isConnected.removeEventListener(
//       'change',
//       this.handleIsConnected
//     );

  }

  // componentDidMount() {
  //   NetInfo.isConnected.fetch().then(isConnected => {
  //     this.handleIsConnected(isConnected);
  //   });
  //   NetInfo.isConnected.addEventListener(
  //     'change',
  //     this.handleIsConnected
  //   );
  // }

  // handleIsConnected(isConnected) {
  //   this.setState({ isConnected });
  // }


  onButtonPress() {
    this.setState({ error: '', info: '' });
    if (this.state.username === '' || this.state.password === '') {
      this.setState({ error: 'login_form_validation_failed' });
    }
    //console.log(this.state.isConnected);
    //if(this.state.isConnected)
    
      loginUser({ username: this.state.username,
                password: this.state.password,
                component: this })
              .then((response) => {
                if (response) 
                  this.props.navigator.replace({ name: 'home' });
              });
    // }   else {
    //   alert("Network Connection Lost!");
    // }
  }

  renderButton() {
    if (this.state.loading) {
      return <Spinner size="large" text={this.props.info} />;
    }
    return (
      <Button onPress={this.onButtonPress.bind(this)} style={'primary-default'}>
        Login
      </Button>
    );
  }

  render() {
    return (
      <View style={{flex:1, justifyContent: 'center', alignItems: 'center' }} >
    
      <View style={{ justifyContent:'center', alignItems: 'center' }}>
          <Image
           source={bgImage}
           style={{ width: 250, height: 50}}
          />
          </View>  
        <InputForm style={ [ SharedStyles.screenContainer, styles.formView ] }>
          
          <InputSection style={styles.inputSectionView}>
            <Input
                  placeholder="Username"
                  value={this.state.username}
                  onChangeText={(text) => { this.setState({ username: text }); }}
            />
          </InputSection>
          <InputSection style={styles.inputSectionView}>
            <Input
                  placeholder="Password"
                  secureTextEntry
                  value={this.state.password}
                  onChangeText={(text) => { this.setState({ password: text }); }}
            />
          </InputSection>
          {this.state.error !== '' &&
            <FormMessage
                        type='error'
                        message={this.state.error}
            />
          }
          <InputSection style={styles.inputSectionView}>
            {this.renderButton()}
          </InputSection>

        </InputForm>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  formView: {
    justifyContent: 'center',
    alignItems: 'center',
    marginLeft: 30,
    marginRight: 30,
    paddingBottom: 10,
    paddingRight: 5,
    paddingLeft: 5,
    borderWidth: 1,
    borderRadius: 5,
    shadowColor: StyleVars.Colors.darkLine,
    shadowRadius: 5
  },
  inputSectionView: {
    backgroundColor: 'rgba(255,255,255,0)'
  }
});
