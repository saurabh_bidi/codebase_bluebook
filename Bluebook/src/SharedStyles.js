import { StyleSheet } from 'react-native';
import StyleVars from './StyleVars';

export default StyleSheet.create({
  errorTextStyle: {
    fontSize: 16,
    alignSelf: 'center',
    color: 'red',
    lineHeight: 23
  },
  screenContainer: {
    backgroundColor: StyleVars.Colors.cardBackground
  }  
});
