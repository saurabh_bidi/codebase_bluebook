//  Import Libraries
import React, { Component } from 'react';
import { View, Text, TextInput, TouchableOpacity, ScrollView, StyleSheet, BackAndroid, NetInfo } from 'react-native';
// import { TabViewAnimated, TabBar } from 'react-native-tab-view';
import {
		 Header, 
		 Button,
         Avatar,
         DateInput,
         Input,
         InputSection,
         Spinner,
         PickerInput,
         ErrorPage 
     } from '../components/common';
import SharedStyles from '../SharedStyles';
import StyleVars from '../StyleVars';
import { submitAnswers, getAnswers, getFormMeta } from '../actions/submitAction';
import { logErrorToAnalytics } from './../Logger';
//import { connectionShape } from 'react-native-connection-info';
import moment from 'moment';
import Constants from '../Constants';

// Components
export default class Main extends Component {

	constructor(props) {
		super(props);	 
		this.questionsPerPage = [0, 10, 17];   		
		this.fieldToQuestionMapping = {
			11:0,
			12:1,
			13:2,
			14:3,
			15:4,
			16:5,
			17:6,
			18:7,
			19:8,
			110:9,
			21:10,
			22:11,
			23:12,
			24:13,
			25:14,
			26:15,
			27:16,
			33:17,
			34:18,
			35:19,
			36:20,
			37:21,
			38:22,
			39:23,
			310:24 
		}
		console.log(this.props);
		const inputStates = [];
		for(let i=0;i< 25;i++) {
			inputStates.push(
				{
					valid: false,
					message: null,
					state: 'enabled'
				}
			)
		}
		inputStates[7].valid = true;
		inputStates[8].valid = true;
		this.state = {
			loading: false,
			index: 0,
			inputStates: inputStates,
			formMeta: null,
			inputStartIndex: 0,
			doctor: this.props.route.doctor,
			surveyId: this.props.route.surveyId,
			date: this.props.route.date,
			routes: [
				{ key: 'rxInfo', title: 'RX Info' },
				{ key: 'ProgInfo', title: 'Program Info' },
				{ key: 'CompData', title: 'Competitor Data'}
			],
			answerData : {
				id: '',
				doctor: props.route.doctor.referenceId,
				rx: '',
				rxPlan:'',
				rxAchived:'',
				leadToDiasol: '',
				diapenRxBed:'',
				sourceOfBusiness:'',
				demoBy:'',
				rxPhoneNo: '',
				lastCmeDate: '',
				lastWosinRXS: '',
				surveyDate: moment(this.props.route.date).format('DD/MM/YYYY'),

				glaritusInitiation:'', 
				glaritusActivityAttended: '', 
				speakerGlaritusProgram:'', 
				glaritusActivityDetail:'',
				oldGlaritusPatients:'', 
				glaritusDropOutPatients:'', 
				dropoutReason:'',
				// patientsInMonth:'', 
				// patientsOnInsulin:'', 
				humanInsulinPTX:'', 
				wosulinRX:'', 
				bioconRX:'',
				lupinRX:'', 
				novoRX:'', 
				otherRX:'', 
				glaritusRX:'',
				glaritusRXed: ''
			},
			labels: {

			}
		}				
	}

	componentWillMount() {
		// if ( !this.state.surveyId || this.state.surveyId === '') {
		// 		this.onChangeTab(0);
		// 		console.log('Resu')
		// 	return;
		// }

		this.setState({ loading: true });		
		console.log(this.state.doctor);
		getAnswers({doctorId:this.state.doctor.referenceId, date: this.state.date}) .then((response) => {
			  console.log(response);
			  if(response.data)
	        	{
				
					const { answer, fieldMapping } = response.data;
				if(this.state.surveyId)	
					{
						//console.log(answerData);
						const answerData = {
							id: '',
							doctor: answer.doctor?answer.doctor.id: this.state.doctor.referenceId,
							rx: answer.rx + '',   
							rxPlan: answer.rxPlan  ,
							rxAchived: answer.rxAchived + '' ,
							leadToDiasol: answer.leadToDiasol + '' ,
							diapenRxBed: answer.diapenRxBed + '' ,
							sourceOfBusiness: answer.sourceOfBusiness + '',
							demoBy: answer.demoBy + '',
							rxPhoneNo: answer.rxPhoneNo + '',
							lastCmeDate: moment(answer.lastCmeDate).format("DD MMM YYYY"),
							lastWosinRXS: answer.lastWosinRXS + '',
							surveyDate : moment(this.state.answerData.date).format('DD/MM/YYYY') + '',

							glaritusInitiation: answer.glaritusInitiation + ''  , 
							glaritusActivityAttended: answer.glaritusActivityAttended + '', 
							speakerGlaritusProgram: answer.speakerGlaritusProgram + '',
							glaritusActivityDetail: answer.glaritusActivityDetail + '',
							oldGlaritusPatients: answer.oldGlaritusPatients + '',
							glaritusDropOutPatients: answer.glaritusDropOutPatients + '',
							dropoutReason: answer.dropoutReason + '',

							humanInsulinPTX: answer.humanInsulinPTX + '' ,
							wosulinRX: answer.wosulinRX + '',
							bioconRX: answer.bioconRX + '' ,
							lupinRX: answer.lupinRX + '' ,
							novoRX: answer.novoRX + '' ,
							otherRX: answer.otherRX + '' ,
							glaritusRX: answer.glaritusRX + '' ,
							glaritusRXed: answer.glaritusRXed + ''
					};
					console.log('Field', fieldMapping);
					let inputStates = this.state.inputStates;
					Object.keys(fieldMapping).forEach((key) => {
						if(this.fieldToQuestionMapping[key]) {
							if(fieldMapping[key] === 1)
							{
								inputStates[this.fieldToQuestionMapping[key]].state =  'enabled';
								inputStates[this.fieldToQuestionMapping[key]].valid =  true;
							} else
							{
								inputStates[this.fieldToQuestionMapping[key]].state =  'disabled';
								inputStates[this.fieldToQuestionMapping[key]].valid =  true;
							}
						}
					});
					// inputStates[0].state = 'disabled';
					inputStates[0].valid = true;
            	this.setState({ fieldMapping : fieldMapping, loading: false, answerData: answerData, inputStates: inputStates });
				}
					else {
						const answerData = this.state.answerData;
						answerData.rxPlan = answer.rxPlan; 
						
					console.log('Field', fieldMapping);
					let inputStates = this.state.inputStates;
					Object.keys(fieldMapping).forEach((key) => {
						if(this.fieldToQuestionMapping[key]) {
							if(fieldMapping[key] === 1)
							{
								inputStates[this.fieldToQuestionMapping[key]].state =  'enabled';
							} else
							{
								inputStates[this.fieldToQuestionMapping[key]].state =  'disabled';
							}
						}
					});
					// inputStates[0].state = 'disabled';
					// inputStates[0].valid = true;
            	this.setState({ fieldMapping : fieldMapping, loading: false, answerData: answerData, inputStates: inputStates });
					}
				console.log(this.state, "getAnswers");
					
				}
		}).catch((e) => console.log(e));
		this.onChangeTab(0);
	}

	componentDidMount() {
		
	}
	isNumeric(value) {
		    if (/^(\-|\+)?([0-9]+(\.[0-9]+)?|Infinity)$/.test(value))
		      return Number.parseFloat(value) || Number.parseFloat(value) == 0.0 ;
		  return NaN;
	}

	isSpecialChar(value) {
		if(/^[@!#\$\^%&*()+=\-\[\]\\\';,\.\/\{\}\|\":<>\? ]+$/.test(value))
		return true;
	}
   
	changeValueCb(value, section) {
		const inputStates = this.state.inputStates;
		const newStates = inputStates;		
		let answers = this.state.answerData;	
		let labels = this.state.labels;	
		let val = null;
		switch (section.no) {
			case 1: 
				 val = value.key;
				 
				let status = val === 'Plan' ? 'disabled' : 'enabled';				
	 			for(let i = 2; i < inputStates.length; i++ ) {
					 if(i==7 || i == 8) 
					 {
						 	newStates[i] = {
									state: status,
								};

						 continue;
					 }
					newStates[i] = {
						state: status,
						valid: status==='disabled'
					};
				}		
				newStates[0] = {
					state : 'enabled',
					valid: true
				}
				// Set All to Default
				answers.rxAchived = '';
				answers.leadToDiasol = '' ;
				answers.diapenRxBed = '';
				answers.sourceOfBusiness = '';
				answers.demoBy = '';
				answers.rxPhoneNo = '';
				answers.lastCmeDate = '';
				answers.lastWosinRXS = '';
				
				answers.glaritusInitiation = '';
				answers.glaritusActivityAttended = ''; 
				answers.speakerGlaritusProgram = '';
				answers.glaritusActivityDetail = '';
				answers.oldGlaritusPatients = '';
				answers.glaritusDropOutPatient = '';
				answers.dropoutReason = '';
				// patientsInMonth:'', 
				// patientsOnInsulin:'', 
				answers.humanInsulinPTX = '';
				answers.wosulinRX = '';
				answers.bioconRX = '';
				answers.lupinRX = '';
				answers.novoRX = '';
				answers.otherRX = '';
				answers.glaritusRX = '';
				answers.glaritusRXed = ''
		
		
				answers[section.answer] = val;
				labels[section.no] = value.label;
			break;
			case 2:
			    if (!this.isNumeric(value) || this.isSpecialChar(value)) {
					newStates[1]= {
						state: 'enabled',
						valid: false,
						message: 'Invalid Data',
					}
				} else {
						newStates[1]= {
						state: 'enabled',
						valid: true,
						message: ''
					}
				}
				answers[section.answer] = value;
			break;
			case 3:	
			if (value === undefined || value === ''|| !this.isNumeric(value) || this.isSpecialChar(value)) {		
					newStates[2]= {
						state: 'enabled',
						valid: false,
						message: 'Invalid Data'
					}
				} else {
						newStates[2]= {
						state: 'enabled',
						valid: true,
						message: ''
					}
				}
				answers[section.answer] = value;
			break;
			case 4:		
			val = value.key
			if( value === undefined || value === '' ) {

				{
						newStates[3]= {
						state: 'enabled',
						valid: false,
						message: 'Invalid Data'
					}
				}
				} else {
						newStates[3]= {
						state: 'enabled',
						valid: true,
						message: ''
					}
				}
			
				answers[section.answer] = val;
				labels[section.no] = value.label;
			break;
			case 5:
			if( value === undefined || value === '' || !this.isNumeric(value) || this.isSpecialChar(value))			
			{	newStates[4]= {
					state: 'enabled',
					valid: false,
					message: 'Invalid Data'
				}
			}else {
						newStates[4]= {
						state: 'enabled',
						valid: true,
						message: ''
					}
				}
				answers[section.answer] = value;
			break;
			case 6:	
			val = value.key;
				if ( value === undefined|| value === '') {
				newStates[5]= {
					state: 'enabled',
					valid: false,
					message: 'Invalid Data'
				}
			}
			else {
						newStates[5]= {
						state: 'enabled',
						valid: true,
						message: ''
					}
				}
				answers[section.answer] = val;
				labels[section.no] = value.label;
			break;
			case 7:	
			val = value.key;
			if( value === undefined || value === '')	
			{	
				newStates[6]= {
					state: 'enabled',
					valid: false,
					message: 'Invalid Data'
				}
			}else {
						newStates[6]= {
						state: 'enabled',
						valid: true,
						message: ''
					}
				}
				answers[section.answer] = val;
				labels[section.no] = value.label;
			break;
			case 8:		
			 if (!this.isNumeric(value) || this.isSpecialChar(value)) {	
				newStates[7]= {
					state: 'enabled',
					valid: false,
					message: 'Invalid Data'
		 	}
			 }else {
						newStates[7]= {
						state: 'enabled',
						valid: true,
						message: ''
					}
				 }
				answers[section.answer] = value;
			break;
			case 9:		
			//if ( value === undefined || value === '') {	
			// 	newStates[8]= {
			// 		state: 'enabled',
			// 		valid: false,
			// 		message: 'Invalid Data'
			// 	}
			// }
			// else {
						newStates[8]= {
						state: 'enabled',
						valid: true,
						message: ''
					}
				// }
				answers[section.answer] = value;
			break;
			case 10:	
			if ( value === undefined || value === ''|| !this.isNumeric(value) || this.isSpecialChar(value)) {		
				newStates[9]= {
					state: 'enabled',
					valid: false,
					message: 'Invalid Data'
				}
			}
			else {
						newStates[9]= {
						state: 'enabled',
						valid: true,
						message: ''
					}
				}
				answers[section.answer] = value;
			break;
			case 11:	
			if( value === undefined || value === '' || !this.isNumeric(value) || this.isSpecialChar(value)){

					
				newStates[10]= {
					state: 'enabled',
					valid: false,
					message: 'Invalid Data'
				}
			}
				else {
						newStates[10]= {
						state: 'enabled',
						valid: true,
						message: ''
					}
				}
				answers[section.answer] = value;
			break;
			case 12:	
			 val = value.key;
				status = val === '0' ? 'disabled' : 'enabled';	
				for(let i=12;i<14;i++) {				
				newStates[i]= {
					state: status,
					valid: status === 'disabled',
					message: ''
				};
				answers.speakerGlaritusProgram = '';
				answers.glaritusActivityDetail = '';
			}
				newStates[11] = {
					state : 'enabled',
					valid: true,
					message: 'Invalid Data'
				}
			answers[section.answer] = val;
				labels[section.no] = value.label;
			
			break;
			case 13:	
			if( value === undefined|| value === '' || this.isNumeric(value) || this.isSpecialChar(value) )	{

				newStates[12]= {
					state: 'enabled',
					valid: false,
					message: 'Invalid Data'
				}
			}
				else {
						newStates[12]= {
						state: 'enabled',
						valid: true,
						message: ''
					}
				}
				answers[section.answer] = value;
			break;
			case 14:
		val = value.key;
			if( value === undefined || value === ''){
		
				newStates[13]= {
					state: 'enabled',
					valid: false,
					message: 'Invalid Data'
				}
			}
				else {
						newStates[13]= {
						state: 'enabled',
						valid: true,
						message: ''
					}
				}
				answers[section.answer] = val;
				labels[section.no] = value.label;r
			break;
			case 15:	
			if ( value === undefined || value === '' || !this.isNumeric(value) || this.isSpecialChar(value)) {		
				newStates[14]= {
					state: 'enabled',
					valid: false,
					message: 'Invalid Data'
				}
			}
			else {
						newStates[14]= {
						state: 'enabled',
						valid: true,
						message: ''
					}
				}
				answers[section.answer] = value;
			break;
			case 16:	
			if ( value === undefined || value === ''|| !this.isNumeric(value) || this.isSpecialChar(value)) {		
				newStates[15]= {
					state: 'enabled',
					valid: false,
					message: 'Invalid Data'
				}
			}
			else {
						newStates[15]= {
						state: 'enabled',
						valid: true,
						message: ''
					}
				}
				answers[section.answer] = value;
			break;
			case 17:
			if( value === undefined || value === '' ||this.isNumeric(value) || this.isSpecialChar(value)){			
				newStates[16]= {
					state: 'enabled',
					valid: false,
					message: 'Invalid Data'
				}
			}
				else {
						newStates[16]= {
						state: 'enabled',
						valid: true,
						message: ''
					}
				}
				answers[section.answer] = value;
			break;
			case 18:	
			if ( value === undefined || value === ''|| !this.isNumeric(value) || this.isSpecialChar(value)) {		
				newStates[17]= {
					state: 'enabled',
					valid: false,
					message: 'Invalid Data'
				}
			}
			else {
						newStates[17]= {
						state: 'enabled',
						valid: true,
						message: ''
					}
				}
				answers[section.answer] = value;
			break;
			case 19:	
			if ( value === undefined || value === ''|| !this.isNumeric(value) || this.isSpecialChar(value)) {		
				newStates[18]= {
					state: 'enabled',
					valid: false,
					message: 'Invalid Data'
				}
			}
			else {
						newStates[18]= {
						state: 'enabled',
						valid: true,
						message: ''
					}
				}
				answers[section.answer] = value;
			break;
			case 20:	
			if ( value === undefined || value === ''|| !this.isNumeric(value) || this.isSpecialChar(value)) {		
				newStates[19]= {
					state: 'enabled',
					valid: false,
					message: 'Invalid Data'
				}
			}
			else {
						newStates[19]= {
						state: 'enabled',
						valid: true,
						message: ''
					}
				}
				answers[section.answer] = value;
			break;
			case 21:	
			if ( value === undefined || value === ''|| !this.isNumeric(value) || this.isSpecialChar(value)) {		
				newStates[20]= {
					state: 'enabled',
					valid: false,
					message: 'Invalid Data'
				}
			}else {
						newStates[20]= {
						state: 'enabled',
						valid: true,
						message: ''
					}
				}
				answers[section.answer] = value;
			break;
			case 22:
			if ( value === undefined || value === ''|| !this.isNumeric(value) || this.isSpecialChar(value)) {			
				newStates[21]= {
					state: 'enabled',
					valid: false,
					message: 'Invalid Data'
				}
			}
			else {
						newStates[21]= {
						state: 'enabled',
						valid: true,
						message: ''
					}
				}
				answers[section.answer] = value;
			break;
			case 23:	
			if ( value === undefined || value === '' || !this.isNumeric(value) || this.isSpecialChar(value)) {		
				newStates[22]= {
					state: 'enabled',
					valid: false,
					message: 'Invalid Data'
				}
			}
			else {
						newStates[22]= {
						state: 'enabled',
						valid: true,
						message: ''
					}
				}
				answers[section.answer] = value;
			break;
			case 24:	
			if ( value === undefined || value === ''|| !this.isNumeric(value) || this.isSpecialChar(value)) {		
				newStates[23]= {
					state: 'enabled',
					valid: false,
					message: 'Invalid Data'
				}
			}
			else {
						newStates[23]= {
						state: 'enabled',
						valid: true,
						message: ''
					}
				}
				answers[section.answer] = value;
			break;
			case 25:	
			if ( value === undefined || value === ''|| !this.isNumeric(value) || this.isSpecialChar(value)) {		
				newStates[24]= {
					state: 'enabled',
					valid: false,
					message: 'Invalid Data'
				}
			}
			else {
						newStates[24]= {
						state: 'enabled',
						valid: true,
						message: ''
					}
				}
				answers[section.answer] = value;
			break;	
		}
		console.log(val);
				//answers[section.answer] = val;		
		this.setState({ inputStates: newStates, answerData: answers, labels: labels});					
	}

	
	renderQuestionSection(section, index) {
		const stateIndex = this.state.inputStartIndex + index;
		const inputState = this.state.inputStates[stateIndex];
		const questionNo = index + 1;
		let keyboardType = 'default';
		if (section.inputType === 'number')
			keyboardType = 'numeric';
		if (section.inputType === 'phone')
			keyboardType = 'phone-pad';
			
			let yes_no = '';
			switch ( this.state.answerData[section.answer] )
			{
				case 0:
				case '0':
							yes_no = 'No';
							break;
				case 1:
				case '1':
							yes_no = 'Yes';
							break;
				default: 
							yes_no = !this.state.answerData[section.answer] || this.state.answerData[section.answer] === 'null' ? '' : this.state.answerData[section.answer]+'';
							break;
			}

		return (
			<View style={{flex: 1, flexDirection: 'column',  paddingTop: 10, paddingLeft: 5}} key={index}>
				<View style={{flex: 1, flexDirection: 'row'}}>
					<Text style={{flex: 1, fontSize: 17 }}>{questionNo}.</Text>	
					<Text style={{flex: 13, fontSize: 17 }}>{section.question}</Text>
				</View>
				<View style={{flex: 1, flexDirection: 'column', borderBottomWidth: 1, borderBottomColor: StyleVars.Colors.lightLine}}>
					{ ( section.inputType==='text' || section.inputType === 'number'
					    || section.inputType === 'phone'  ) && inputState.state === 'enabled' &&
						<Input
							value = { !this.state.answerData[section.answer] || this.state.answerData[section.answer] === 'null' ? '' : this.state.answerData[section.answer] + '' }
							keyboardType={keyboardType}
						 	onChangeText = { (value) => this.changeValueCb(value, section)} />
					}
					{ section.inputType==='picker' && inputState.state === 'enabled' &&
						<PickerInput
							data={ section.options }
							value = { this.state.answerData[section.answer] + '' }
					  		displayText = { yes_no }  //{ this.state.answer[dataAstemp] ? this.state.answer[dataAstemp].label : '' }                    
							onChangeText= { (option) => {			
									console.log(option);					    																		
									this.changeValueCb(option, section)
								}
							}	
						/>
					}
					{ section.inputType==='date' &&  inputState.state === 'enabled' &&
						<DateInput
							format= {'DD MMM YYYY'}
							value = { !this.state.answerData[section.answer] || this.state.answerData[section.answer] === 'null' ? '' : this.state.answerData[section.answer] + '' }
							onChangeText= { (value) => {			
													console.log(value);    	 																		
									this.changeValueCb(value, section)
								}
							}
		                />
						
					}
					{ inputState.state === 'disabled' &&
						<Text style= {{ paddingLeft: 15 }}> {this.state.answerData[section.answer]} </Text>
					}
					{ inputState.valid === false &&
						<Text style= {{ color: '#FF0000' }}>{inputState.message}</Text>
					}
				</View>
			</View>
		)
	}

	// renderTabBar = (props) => {
	// 	return <TabBar {...props} />;
	// };

	renderScene() {	
		return (
  		 <ScrollView
  		 	ref={(scrollView) => { _scrollView = scrollView; }} 
  		 	automaticallyAdjustContentInsets={false} 
  		 	onScroll={() => { console.log('onScroll!'); }} 
  		 	scrollEventThrottle={200} style={[styles.scrollView, {flex:1, padding: 4, marginTop: 2}]}
  		 	>
			   { 
				   this.state.formMeta.map(this.renderQuestionSection.bind(this))
			   }
		 </ScrollView>	   
		);

	};

	onChangeTab(index) {
		console.log(index);
		let inputStartIndex = 0;
		switch (index) {
		case 0:
			formMeta = getFormMeta('RxInfo');
			inputStartIndex = 0;
			break;
		case 1:
			formMeta = getFormMeta('ProgInfo');
			inputStartIndex = 10;
			break;	
		case 2:
			formMeta = getFormMeta('CompData');
			inputStartIndex = 17;
			break;
		default:
			formMeta = null;
		}
		this.setState({
			formMeta: formMeta,
			inputStartIndex: inputStartIndex,
			index: index
		});
	}

	onSubmitPress() {
		console.log('Input states in submit', this.state.inputStates);
		let isValid = true;
		let instates = this.state.inputStates;
		instates.forEach((isState) => {
			if (isState.valid === false && isState.state === 'enabled')
				{
					isValid = false;
					if(!isState.message && isState.state === 'enabled' ) isState.message = 'Invalid Data';
				}
		});
		this.setState({ inputState: instates });
		console.log('Input states in submit', isValid, this.state.answerData.lastCmeDate);
		if (isValid)
			{					
				submitAnswers({answer: this.state.answerData, component: this});
			} else {
				alert('Please fill all the Mandatory fields.');
			}
	}

	renderHeader() {
		if ( !this.state.surveyId || this.state.surveyId === '') {
		return (
			<Header 
				headerText={this.props.route.doctor.name}
				leftText = {'Back'}
				onLeftPress={() => {this.props.navigator.replace({ name: 'home', date: this.state.date });}}
				rightText = {'Submit'}
				onRightPress = { this.onSubmitPress.bind(this)}
				
			/>
		);
		} else
		{
			return (
				<Header 
					headerText={this.props.route.doctor.name}
					leftText = {'Back'}
					onLeftPress={() => {this.props.navigator.replace({ name: 'home', date: this.state.date });}}		
				/>
			)
		}
	}

	tabs(){
		let style1 = {  borderBottomColor: '#EFEFF4', borderBottomWidth: 5 }, 
			style2 = { borderBottomColor: '#EFEFF4', borderBottomWidth: 5 }, 
			style3 = { borderBottomColor: '#EFEFF4', borderBottomWidth: 5 };
		switch(this.state.index) {
			case 0:
					style1 = {  }; 
					break;	
			case 1:
					style2 = {  }; 
					break;
			case 2:
					style3 = {  }; 
					break;		
		}
		return(
			<View style={ { height: 50, flexDirection: 'row', justifyContent: 'center', alignItems: 'stretch', backgroundColor: '#3186F3', marginTop: 1 } }>
				<TouchableOpacity onPress={this.onChangeTab.bind(this,0)} style={[{ flex:1, justifyContent: 'center', borderColor: 'white', borderWidth: 0.25}, style1]}>
					<Text style={{ color: 'white', alignSelf: 'center' }}>
						RX INFO
					</Text>
				</TouchableOpacity>
				<TouchableOpacity onPress={this.onChangeTab.bind(this,1)} style={[{ flex:1, justifyContent: 'center', borderColor: 'white', borderWidth: 0.25}, style2]}>
					<Text style={{ color: 'white', alignSelf: 'center' }}>
						PROGRAM INFO
					</Text>
				</TouchableOpacity>
				<TouchableOpacity onPress={this.onChangeTab.bind(this,2)} style={[{ flex:1, justifyContent: 'center', borderColor: 'white', borderWidth: 0.25}, style3]}>
					<Text style={{ color: 'white', alignSelf: 'center', padding:4 }}>
						COMPETITOR DATA
					</Text>
				</TouchableOpacity>
			</View>
		);
	}
   
   _Spinner(){
	      	 	if (this.state.loading) 
      				{
      					return (
      					<View style = { { flex:1, justifyContent: 'center', alignItems:'center' } } >
      					<Spinner size="large" text={this.props.info} />
      					</View >
      					);
      				}
      				else
  					{
  						return(
							  <View style={{flex:1}}> 
							  	{
									this.tabs()
								}
								
								{
									this.renderScene()
								}
							  </View>
								
			      		
			      );
			      // </>
  					}
		}

	render() {
		const { mainContainer } = styles;
		console.log(this.state.inputStates);
		return (
				<View style={{ backgroundColor: '#EFEFF4', flex: 1 }}>									
								{
									this.renderHeader()
								}
								{
								this._Spinner()
								}
							</View>
		);
	}

  }

const styles = StyleSheet.create({
	mainContainer: {
		flex:1,
		
	}
});