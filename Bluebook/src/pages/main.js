//  Import Libraries
import React, { Component } from 'react';
import { View, Text, TextInput,  ListView, ScrollView, StyleSheet, TouchableHighlight, Image, NetInfo, TouchableOpacity} from 'react-native';
import { TabViewAnimated, TabBar } from 'react-native-tab-view';
import DatePicker from 'react-native-datepicker';
import moment from 'moment';
import HttpUtils from './../HttpUtils';

import Constants from './../Constants';
import { Header, Button, Spinner } from '../components/common';
import bgImage from '../images/done1.png';
//import { connectionShape } from 'react-native-connection-info';
import { getDoctorsForDate } from './../actions/LoginActions';
 
// Components

export default class Main extends Component {

	constructor(props) {
			super(props);
			let date = this.props.route.date || new Date();
			this.ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});				
			this.state = {
				dataSource: this.ds.cloneWithRows([]),
				//isConnected: false,
				doctors: null,
				date: date
			}	
			//this.handleIsConnected = this.handleIsConnected.bind(this);
			
	}
			
	componentWillMount() {
		this.setState({ loading: true});  
		getDoctorsForDate(this.state.date)
		.then((bootstrap) => {
			console.log(bootstrap);
			this.setState({
				doctors: bootstrap.data.doctors,
				loading: false,
				dataSource: this.ds.cloneWithRows(bootstrap.data.doctors)
			})
		}).catch((e) => {
			//TODO: show error;
		})
	}


	// componentDidMount() {
	// 	NetInfo.isConnected.fetch().then(isConnected => {
	// 	this.handleIsConnected(isConnected);
	// 	});
	// 	NetInfo.isConnected.addEventListener(
	// 	'change',
	// 	this.handleIsConnected
	// 	);
	// }

	// handleIsConnected(isConnected) {
	// 	this.setState({ isConnected });
	// }


_pressRow(rowData, rowID) { 
	console.log(rowData, rowID);
	// if(this.state.isConnected)
		
			this.props.navigator.replace({ name: 'mainpage', doctor: { name: rowData.name, referenceId : rowData.id},  surveyId: rowData.surveyId, date: this.state.date });
		// } else
		// {
		// 	alert("Network Connection Lost!");
		// }
}

isDone(rowData) {
	if(rowData.surveyId !== undefined && rowData.surveyId !== '' )
	{
		return (
			<Image
					source={bgImage}
					style={{ width: 25, height: 25}}
					/>
			);
	}
}

_CapitalizeFirstLatter( name ){
	var temp = '';
	var array = name.split(' ');
	array.map((txt) => {
			temp += txt.charAt(0).toUpperCase() + txt.substr().toLowerCase() + ' ';
			console.log(txt);
			console.log(temp); 
	});
	console.log(temp);
}
	
onButtonPress() {	
	//Constants.Session["doctors"]=[];
	this.setState({ loading: true});  
		getDoctorsForDate(this.state.date)
		.then((bootstrap) => {
			this.setState({
				doctors: bootstrap.data.doctors,
				loading: false,
				dataSource: this.ds.cloneWithRows(bootstrap.data.doctors)
			})
		}).catch((e) => {
			//TODO: show error;
		});
			
} 
_Spinner(){
	      	 	if (this.state.loading) 
      				{
      					return (
      					<View style = { { flex:1, justifyContent: 'center', alignItems:'center' } } >
      					<Spinner size="large" text={this.props.info} />
      					</View >
      					);
      				}
      				else
  					{
  						return(
							<ListView
										dataSource={this.state.dataSource}
										renderRow={(rowData, sectionID, rowID) => {
											return (
											<View >
												<TouchableHighlight onPress={() => { this._pressRow(rowData, rowID) }}>
													<View style={ styles.item } >
													<View style={{ flex:1, flexDirection:'row' }}>
														<View style={{ flex:9 }}>
														<View style={{flexDirection: 'row', alignItems: 'center'}}>
															<Text style= { styles.textItemHeader }>{rowData.name} </Text>
															<Text style= {{ fontSize:12, color: '#5a5a5a' }}> ({rowData.code}) </Text>
														</View>
														
														<Text style= {{fontSize:12, color: '#5a5a5a'}}>{rowData.speciality}</Text>
														</View>
														<View style={{ flex:1, justifyContent: 'center', alignItems: 'center' }}>
															{ this.isDone(rowData) }
														</View>
														</View>
													</View>
												</TouchableHighlight>
												</View>
												);
											}
								}
										/>
								
			      		
			      );
			      // </>
  					}
		}


	render() {
		return (
      <View style={{ backgroundColor: '#EFEFF4', flex: 1 }}>
      	<View  >
          <Header headerText={'Select Doctor'} />
        </View>
		<View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between'}}>
			
				<DatePicker
					style={{ flex:1, marginTop: 10 }}
					date={this.state.date}
					mode="date"
					placeholder="select date"
					format="DD-MM-YYYY"
					minDate="2017-01-01"
					maxDate="2017-31-12"
					confirmBtnText="Confirm"
					cancelBtnText="Cancel"
					customStyles={{
					dateIcon: {
						position: 'absolute',
						left: 0,
						top: 4,
						marginLeft: 10
				},
				dateInput: {
					marginLeft: 10,
				}
				// ... You can check the source to find the other keys.
				}}
				onDateChange={(date) => {this.setState({date: moment(date, 'DD/MM/YYYY').toDate()})}}
			/>
			<View style={{marginTop: 10, marginLeft: 10, }}>
				<TouchableOpacity style={{width: 80, backgroundColor: '#2196F3',height: 30, borderRadius: 4, marginRight: 10}} onPress={this.onButtonPress.bind(this)} >
					<Text style={{color:'#fff', textAlign: 'center', marginTop: 5}}>Search</Text>
					</TouchableOpacity>
			</View>
			
		</View>
        { this._Spinner() }
      </View>
    );
	}
}

const styles = StyleSheet.create({
	mainHeader: {
		flex:1,
		maxHeight: 22
	},
	mainContainer: {
		flex:1,
		alignItems: 'stretch',
		borderColor: 'red',
		marginTop: 22,
		backgroundColor: '#e6e6e6'
	},
	textItemHeader: {
		fontSize:16,
		color: '#050505',
		alignItems:'stretch',
		
	},
	item: {
		flex:1,
		justifyContent: 'center',
		alignItems: 'stretch',
		minHeight: 45,
		backgroundColor: "#FFFFFF",
	    borderRadius: 2,
	    shadowColor: "#9AA0A0",
	    shadowOpacity: 0.3,
	    marginTop: 3, 
	    marginBottom: 2, 
	    padding: 10,
	    alignItems:'stretch',
	    shadowRadius: 1,
	    shadowOffset: {
	      height: 1,
	      width: 0.3,
	    },
	},

});
