import Login from './Login';
import Home from './pages/main';
import MainPage from './pages/surveyMainPage';

export default { ROUTES:
  {
    login: Login,
    home: Home,
    mainpage: MainPage
  }
};
