export default {
  Colors: {
    primary: '#2a3744',
    secondary: '#82b541',
    navBarBackground: '#2a3744',
    navBarTitle: 'white',
    lightBackground: '#fafafa',
    mediumBackground: '#f0f0f0',
    darkBackground: '#c8c8c8',
    cardBackground: '#EFEFF4',
    grayFont: '#646464',
    blueFont: '#1880fb',
    lightLine: '#dddddd',
    darkLine: '#c8c8c8',
    yellowButton: '#da902b'
  },

  Fonts: {
    logo: 'Arial',
    general: 'Arial'
  }
};
