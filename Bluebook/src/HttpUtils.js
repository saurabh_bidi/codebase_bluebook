import axios from 'axios';

const axiosConfig = axios.create({
  baseURL: 'http://infinity2.wockhardt.com/webapp/rest/v1.0/mobile', 
  //  baseURL: 'http://172.16.1.31:8080/webapp/rest/v1.0/mobile',
  timeout: 30000,
  headers: {
    'CONSUMER_KEY': 'TENAT00000000000000000000000000000001',
    'Content-Type': 'application/json; charset=UTF-8'
  }
});


class HttpUtils {


  static printDetails(a,b,c) {
    // console.log(a , b , c);
    // console.log(axiosConfig);
  }

  static setHeader(header, value) {
      axiosConfig.defaults.headers.common[header] = value;
  }

  static get(url) {
    console.log(url, axiosConfig.defaults);
    HttpUtils.printDetails(url);
    return axiosConfig.get(url);
  }

  static post(url, data) {
        HttpUtils.printDetails(url, data);
        return axiosConfig.post(url, data);
  }

  static put(url, data) {
          HttpUtils.printDetails(url, data);
          return axiosConfig.put(url, data);
  }

  static all(requests) {
        HttpUtils.printDetails(requests);
        return axios.all(requests);
  }

  static spread(callback) {
        HttpUtils.printDetails(callback);
        return axios.spread(callback);
  }

}

export default HttpUtils;
