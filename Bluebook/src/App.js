import React, { Component } from 'react';
import { 
          StyleSheet,
          BackAndroid,
          Navigator 
       } from 'react-native';
import Routes from './Routes';

let route = { name : ''};
let navigator = { };
class App extends Component {


   componentWillMount() {
       BackAndroid.addEventListener('hardwareBackPress', () => {
        // if (this.props.navigator && this.props.navigator.getCurrentRoutes().length > 1) {
        //   this.props.navigator.pop();
          // return true;
        // }
        // return false;
        console.log(this.route);
        if(this.route.name === 'mainpage')
        { 
          this.navigator.replace({ name: 'home' });
          return true;
        }
        else
        return false;
      });
   }

  renderScene(route, navigator) {
    this.route = route;
    this.navigator = navigator;
    const { ROUTES } = Routes;
    const RouteComponent = ROUTES[route.name];
    return <RouteComponent route={route} navigator={navigator} />;
  }

  render() {
    console.disableYellowBox = true;
    return (
      <Navigator
        style={styles.container}
        initialRoute={{ name: 'login' }}
        renderScene={this.renderScene.bind(this)}
        configureScene={() => {
          return Navigator.SceneConfigs.FloatFromRight;
        }}
      />
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  }
});
export default App;
