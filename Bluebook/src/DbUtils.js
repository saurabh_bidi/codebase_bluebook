import Realm from 'realm';

const SCHEMA_VERSION = 0;

class LoginInfo {

}
LoginInfo.schema = {
  name: 'LoginInfo',
  primaryKey: 'id',
  properties: {
    name: 'string',
    certificate: 'string',
    userId: 'string',
    ownerId: 'string',
    id: 'int'
  }
};

class Doctors{

}
Doctors.schema = {
  name: 'Doctors',
  primaryKey: 'code',
  properties: {
      addressLine1 : { type : 'string', optional : true },
      addressLine2 : { type : 'string', optional : true },
      addressLine3 : { type : 'string', optional : true },
      beatRefId : 'string',
      city : { type : 'string', optional : true },
      classification : 'string',
      code : 'string',
      emailId : { type : 'string', optional : true },
      id : 'int',
      name : 'string',
      operation : 'int',
      phone1 : { type : 'string', optional : true },
      phone2 : { type : 'string', optional : true },
      referenceId : 'string',
      speciality : 'string',
      synced : 'int'
  }
}

class NameValuePair {

}
NameValuePair.schema = {
  name: 'NameValuePair',
  primaryKey: 'id',
  properties: {
    value: 'string',
    name: 'string',
    displayOrder: 'int',
    type: 'string',
    id: 'int'
  }
};

class Sequence {

}
Sequence.schema = {
  name: 'Sequence',
  primaryKey: 'name',
  properties: {
    name: 'string',
    value: 'int',
  }
};

const realm = new Realm({
  schema: [Sequence, LoginInfo, NameValuePair, Doctors],
  schemaVersion: SCHEMA_VERSION
});


class DbUtils {
  static save(schema, props) {
    let saved;
    realm.write(() => {
      const obj = { ...props };

      if (obj.id === undefined) {
        let seq = realm.objects('Sequence').filtered(`name = "${schema}"`)[0];
        if (seq === undefined) {
          seq = realm.create('Sequence', { name: schema, value: 0 });
        }
        const id = seq.value + 1;
        seq.value = id;
        obj.id = id;
      }
      saved = realm.create(schema, obj, true);
    });
    return { ...saved };
  }

  static deleteAll() {
    let deleted;
    realm.write(() => {
      deleted = realm.deleteAll();
    });
    return { ...deleted };
  }

  static select(entity, filter) {
    if (filter === undefined) {
      return realm.objects(entity);
    }
    return realm.objects(entity).filtered(filter);
  }
}


export default DbUtils ;
