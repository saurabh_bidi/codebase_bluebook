 import HttpUtils from './../HttpUtils';
import { logErrorToAnalytics } from './../Logger';
// import DbUtils from './../DbUtils';
import DeviceInfo from 'react-native-device-info';
import Constants from '../Constants';

export const loginUser = ({ username, password, component }) => {

  component.setState({ loading: true });
  return HttpUtils.post('/login', {
            username,
            password
          }).then((response) => {
            //console.log(response);
            if (response.status === 204) {
                component.setState({ error: 'invalid_auth', loading: false });
                return false;
            }
            //console.log(response);
            if(response.data !== undefined)
            {
                  HttpUtils.setHeader('AUTH_CERTIFICATE', response.data.certificate);
                  HttpUtils.setHeader('DEVICE_NAME', DeviceInfo.getModel());
                  HttpUtils.setHeader('DEVICE_TYPE', 'mobile');
                  HttpUtils.setHeader('APP_VERSION', DeviceInfo.getVersion());
                  HttpUtils.setHeader('DEVICE_ID', DeviceInfo.getDeviceId());
                  return true;

          }
        }).catch((error) => {
            logErrorToAnalytics('loginUser', error);
            console.log(error);
            component.setState({ error: 'unexpected_error', loading: false });
            return false;
        });

};


export const getDoctorsForDate = (date) => {
  const month = date.getMonth()+1;
  console.log('getDoctorsForDate');
  console.log(date.getMonth());
  console.log(HttpUtils.get('/survey/getdoctorsBluebook/'+date.getDate()+'/'+date.getMonth()+'/'+date.getFullYear()));
    return HttpUtils.get('/survey/getdoctorsBluebook/'+date.getDate()+'/'+date.getMonth()+'/'+date.getFullYear());
}