import HttpUtils from './../HttpUtils';
import { logErrorToAnalytics } from './../Logger';
// import DbUtils from './../DbUtils';
import moment from 'moment';

export const formMeta = {
  'RxInfo': [
    {
      no: 1,
      question: 'Rx',
      inputType: 'picker',
      initValue: '',
      answer: 'rx',
      options: [{key:'Glaritus 30/70', label:'Glaritus 30/70'}, {key:'Glaritus R',  label: 'Glaritus R'}, {key:'Plan', label:'Plan'}]
    },

    {
      no: 2,
      question: 'How many new Rx planned?',
      inputType: 'number',
      answer: 'rxPlan',
      initValue: '0'
    },

    {
      no: 3,
      question: 'How many new Rx achieved?',
      inputType: 'number',
      initValue: '0',
      answer: 'rxAchived'
    },

    {
      no: 4,
      question: 'What was the lead given to Diasol?',
      inputType: 'picker',
      initValue: '0',
      options: [{key:'1', label:'Yes'}, {key:'0',  label: 'No'}],
      answer: 'leadToDiasol'
    },

    {
      no: 5,
      question: 'How many Diapen Rxbed?',
      inputType: 'number',
      initValue: '0',
      answer: 'diapenRxBed'
    },

    {
      no: 6,
      question: 'Source of Business',
      inputType: 'picker',
      initValue: '0',
      answer: 'sourceOfBusiness',
      options: [{key:'Insulin Naive Patients', label:'Insulin Naive Patients'}, {key:'Vials',  label: 'Vials'}, {key: '100IU cartridges', label: '100IU cartridges'}],
    },

    {
      no: 7,
      question: 'Who has given the Demo?',
      inputType: 'picker',
      initValue: '0',
      answer: 'demoBy',
      options: [{key:'TM', label:'TM'}, {key:'DiaSol',  label: 'DiaSol'}, {key:'Stockest Boy',  label: 'Stockest Boy'}, {key:'Chemist',  label: 'Chemist'}, {key:'Paramedic  staff',  label: 'Paramedic  staff'}],
    },

    {
      no: 8,
      question: 'Phone number of RxER',
      inputType: 'phone',
      initValue: '0',
      answer: 'rxPhoneNo'
    },

    {
      no: 9,
      question: 'Last attended CME Date',
      inputType: 'date',
      initValue: '0',
      answer: 'lastCmeDate'
    },

    {
      no: 10,
      question: 'Last Month Wosulin Rxs from the Dr',
      inputType: 'number',
      initValue: '0',
      answer: 'lastWosinRXS'
    },

      

  ],
  'ProgInfo': [
    {
      no: 11,
      question: 'TDD of Glaritus Rxbed in IU',
      inputType: 'number',
      initValue: '0',
      answer: 'glaritusInitiation'      
    },
    {
      no: 12,
      question: 'Has the planned doctor attended Glaritus Program?',
      inputType: 'picker',
      initValue: '0',
      answer: 'glaritusActivityAttended',
      options: [{key:'1', label:'Yes'}, {key:'0',  label: 'No'}],      
    },
    {
      no: 13,
      question: 'Who was the speaker in this program?',
      inputType: 'text',
      initValue: '0',
      answer: 'speakerGlaritusProgram'      
    },
    {
      no: 14,
      question: 'Planned doctor attended Glaritus program as',
      inputType: 'picker',
      initValue: '0',
      answer: 'glaritusActivityDetail' ,
      options: [{key:'Speaker', label:'Speaker'}, {key:'Delegate',  label: 'Delegate'}, {key:'Chairperson', label: 'Chairperson'}],     
    },
    {
      no: 15,
      question: 'How many old Glaritus patients are continuing?',
      inputType: 'number',
      initValue: '0',
      answer: 'oldGlaritusPatients'      
    },
    {
      no: 16,
      question: 'How many old Glaritus patients are stopped?',
      inputType: 'number',
      initValue: '0',
      answer: 'glaritusDropOutPatients'      
    },
    {
      no: 17,
      question: 'Reason for Dropout',
      inputType: 'text',
      initValue: '0',
      answer: 'dropoutReason'      
    },

  ],
  
  'CompData':[
      {
      no: 18,
      question: 'Human insulin Ptx',
      inputType: 'number',
      initValue: '0',
      answer: 'humanInsulinPTX'      
    },
    {
      no: 19,
      question: 'Wosulin Rx',
      inputType: 'number',
      initValue: '0',
      answer: 'wosulinRX'      
    },
    {
      no: 20,
      question: 'Biocon Rx',
      inputType: 'number',
      initValue: '0',
      answer: 'bioconRX'      
    },
    {
      no: 21,
      question: 'Lupin Rx',
      inputType: 'number',
      initValue: '0',
      answer: 'lupinRX'      
    },
    {
      no: 22,
      question: 'Novo Rx',
      inputType: 'number',
      initValue: '0',
      answer: 'novoRX'      
    },
    {
      no: 23,
      question: 'Others Rx',
      inputType: 'number',
      initValue: '0',
      answer: 'otherRX'      
    },
    {
      no: 24,
      question: 'Glaritus Rx',
      inputType: 'number',
      initValue: '0',
      answer: 'glaritusRX'      
    },
    {
      no: 25,
      question: 'Glaritus Rxed',
      inputType: 'number',
      initValue: '0',
      answer: 'glaritusRXed'      
    },

  ]
}

export const getFormMeta = (tabName) => {
  const selectedTab = tabName || 'RxInfo';
  return formMeta[selectedTab];
}


export const submitAnswers = ({ answer, component }) => {

  component.setState({ loading: true });
  answer.surveyDate = moment(component.state.date).format('DD/MM/YYYY');
  let answerData = {

                    id: answer.id ? answer.id : '',
                    doctor: answer.doctor,
                    rx: answer.rx ? answer.rx : null,
                    rxPlan: answer.rxPlan || answer.rxPlan !== '' ? answer.rxPlan : null,
                    rxAchived: answer.rxAchived || answer.rxAchived !== '' ? answer.rxAchived : null,
                    leadToDiasol: answer.leadToDiasol || answer.leadToDiasol !== '' ? answer.leadToDiasol : null,
                    diapenRxBed: answer.diapenRxBed || answer.diapenRxBed !== '' ? answer.diapenRxBed : null,
                    sourceOfBusiness: answer.sourceOfBusiness ? answer.sourceOfBusiness : null,
                    demoBy: answer.demoBy ? answer.demoBy : null,
                    rxPhoneNo: answer.rxPhoneNo ? answer.rxPhoneNo : null,
                    lastCmeDate: answer.lastCmeDate && answer.lastCmeDate !== 'Invalid date'? answer.lastCmeDate : null,
                    lastWosinRXS: answer.lastWosinRXS ? answer.lastWosinRXS : null,
                    surveyDate: answer.surveyDate || answer.surveyDate !== null ? answer.surveyDate : moment(component.state.answerData.date).format('DD/MM/YYYY')  ,

                    glaritusInitiation: answer.glaritusInitiation || answer.glaritusInitiation !== '' ? answer.glaritusInitiation : null, 
                    glaritusActivityAttended: answer.glaritusActivityAttended || answer.glaritusActivityAttended !== '' ? answer.glaritusActivityAttended : null, 
                    speakerGlaritusProgram: answer.speakerGlaritusProgram ? answer.speakerGlaritusProgram : null, 
                    glaritusActivityDetail: answer.glaritusActivityDetail ? answer.glaritusActivityDetail : null,
                    oldGlaritusPatients: answer.oldGlaritusPatients || answer.oldGlaritusPatients !== '' ? answer.oldGlaritusPatients : null, 
                    glaritusDropOutPatients: answer.glaritusDropOutPatients || answer.glaritusDropOutPatients !== '' ? answer.glaritusDropOutPatients : null, 
                    dropoutReason: answer.dropoutReason || answer.dropoutReason !== '' ? answer.dropoutReason : null,
                    // patientsInMonth:'', 
                    // patientsOnInsulin:'', 
                    humanInsulinPTX: answer.humanInsulinPTX || answer.humanInsulinPTX !== '' ? answer.humanInsulinPTX : null, 
                    wosulinRX: answer.wosulinRX || answer.wosulinRX !== '' ? answer.wosulinRX : null, 
                    bioconRX: answer.bioconRX || answer.bioconRX !== '' ? answer.bioconRX : null,
                    lupinRX: answer.lupinRX || answer.lupinRX !== '' ? answer.lupinRX : null, 
                    novoRX: answer.novoRX || answer.novoRX !== '' ? answer.novoRX : null, 
                    otherRX: answer.otherRX || answer.otherRX !== '' ? answer.otherRX : null, 
                    glaritusRX: answer.glaritusRX || answer.glaritusRX !== '' ? answer.glaritusRX : null,
                   glaritusRXed: answer.glaritusRXed || answer.glaritusRXed !== '' ? answer.glaritusRXed : null
 
               };
    console.log(answer);
    answer = answerData;
  return HttpUtils.post('/survey/bluebook', {
            answer
          }).then((response) => {
            if (response.status === 204) {
                component.setState({ error: 'invalid_auth', loading: false });
                return false;
            }
            console.log(response);
            if(response.data && response.data.status)
            {
                       component.setState({ loading: false });
                       alert('Data Saved.');
                      return true;
            }
        }).catch((error) => {
            logErrorToAnalytics('submitAnswers', error);
            component.setState({ error: 'unexpected_error', loading: false });
            alert('unexpected_error occured, please try after sometime.');
            return false;
        });
};

export const getAnswers = ({ doctorId, date }) => {
  //component.setState({ loading: true });
  console.log("getAnswers");
  console.log(date.getMonth());
  return HttpUtils.get('/survey/getanswerBluebook/'+doctorId+'/'+ date.getDate()+'/'+(date.getMonth())+'/'+date.getFullYear()); 
};