import React from 'react';
import { Switch, View, Text } from 'react-native';
import StyleVars from './../../StyleVars';

const Toggle = ({ label, value, onValueChange }) => {
  const { labelStyle,
          inputWrapperWithLabel,
          inputWrapperWithoutLabel,
          inputWithLabel,
          viewStyle,
          inputWithoutLabel, } = styles;

    const inputStyle = label ? inputWithLabel : inputWithoutLabel;
    const inputWrapperStyleToApply = label ? inputWrapperWithLabel : inputWrapperWithoutLabel;
    const height = 40;
    return (
      <View style={viewStyle}>
      { label !== undefined &&
        <Text style={labelStyle}>{label}</Text>
      }
        <View style={inputWrapperStyleToApply}>
            <Switch
              onTintColor="rgb(117, 178, 132)"
              thumbTintColor={StyleVars.Colors.lightLine}
              tintColor="#ff0000"
              value={value}
              onValueChange={onValueChange}
            />
        </View>
      </View>
    );
};


const styles = {
  viewStyle:{
    height: 40,
    flex: 1,
    borderBottomColor: StyleVars.Colors.lightLine,
    borderBottomWidth: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center'
  },
  inputWithoutLabel: {
    flex: 1,
    height: 40,
    fontSize: 16,
    padding: 5,
    color: StyleVars.Colors.blueFont,
    lineHeight: 23
  },
  inputWithLabel: {
    color: StyleVars.Colors.blueFont,
    paddingRight: 5,
    paddingLeft: 5,
    fontSize: 16,
    lineHeight: 23,
    flex: 2,
    fontWeight: '300'
  },
  labelStyle: {
    fontSize: 12,
    paddingLeft: 20,
    flex: 1,
    fontWeight: '300',
  },
  containerStyle: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center'
  },
  inputWrapperWithoutLabel: {
    borderBottomWidth: 1,
    flex: 1,
    borderColor: StyleVars.Colors.lightLine,
    paddingLeft: 10
  },
  inputWrapperWithLabel: {
    flex: 3,
    borderLeftWidth: 1,
    borderColor: StyleVars.Colors.lightLine,
    paddingLeft: 10
  }
};

export { Toggle };
