import React from 'react';
import { View,
         Text } from 'react-native';
import moment from 'moment';
import DatePicker from 'react-native-datepicker';
import StyleVars from './../../StyleVars';

const DateInput = ({ label,
                     value,
                     onChangeText,
                     placeholder,
                     minDate,
                     maxDate,
                     format }) => {
  const { labelStyle,
          containerStyle,
          inputWrapperWithLabel,
          inputWrapperWithoutLabel,
          } = styles;

    const inputWrapperStyleToApply = label ? inputWrapperWithLabel : inputWrapperWithoutLabel;
    return (
      <View style={[containerStyle]}>
      { label !== undefined &&
        <Text style={[labelStyle]}>{label}</Text>
      }
        <View style={inputWrapperStyleToApply}>
        <DatePicker
            date={value}
            mode="date"
            placeholder={placeholder}
            format={format}
            minDate={minDate}
            maxDate={maxDate}
            confirmBtnText="Confirm"
            cancelBtnText="Cancel"
            showIcon={false}
            onDateChange={onChangeText}
            customStyles={{
              dateInput: {
                paddingLeft: 10,
                borderWidth: 0,
              }
            }}
        />
        </View>
      </View>
    );
    // color: StyleVars.Colors.blueFont
};


const styles = {
  inputWithoutLabel: {
    flex: 1,
    height: 40,
    fontSize: 16,
    padding: 5,
    color: StyleVars.Colors.blueFont,
    lineHeight: 23
  },
  datePicker: {
    paddingRight: 5,
    paddingLeft: 5,
    flex: 2
  },
  inputWithLabel: {
    color: StyleVars.Colors.blueFont,
    paddingRight: 5,
    paddingLeft: 5,
    fontSize: 16,
    lineHeight: 23,
    flex: 2,
    fontWeight: '300'
  },
  labelStyle: {
    fontSize: 12,
    paddingLeft: 20,
    flex: 1,
    fontWeight: '100'
  },
  containerStyle: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center'
  },
  inputWrapperWithoutLabel: {
    borderBottomWidth: 1,
    flex: 1,
    borderColor: StyleVars.Colors.lightLine,
    paddingLeft: 10
  },
  inputWrapperWithLabel: {
    flex: 3,
    borderLeftWidth: 1,
    borderColor: StyleVars.Colors.lightLine,
    paddingLeft: 10
  }
};

export { DateInput };
