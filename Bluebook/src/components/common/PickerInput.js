import React from 'react';
import { View,
         TextInput,
         Text } from 'react-native';
import ModalPicker from 'react-native-modal-picker';
import StyleVars from './../../StyleVars';

const PickerInput = ({ label,
                     data,
                     value,
                     displayText,
                     onChangeText,
                     placeholder
                    }) => {
  const { labelStyle,
          containerStyle,
          inputWrapperWithLabel,
          inputWrapperWithoutLabel,
          } = styles;
    // console.log(value, displayText);

    const inputWrapperStyleToApply = label ? inputWrapperWithLabel : inputWrapperWithoutLabel;

    return (
      <View style={[containerStyle]}>
      { label !== undefined &&
        <Text style={[labelStyle]}>{label}</Text>
      }
        <View style={inputWrapperStyleToApply}>
        <ModalPicker
                    data={data}
                    initValue={value}
                    onChange={(option) => { return onChangeText(option); }}
        >
            <TextInput
                style={{ borderWidth: 0,
                         borderColor: '#ccc',
                         padding: 10,
                         height: 40,
                         fontWeight: '100',
                         fontSize: 16 }}
                editable={false}
                value={displayText}
                placeholder={placeholder}
            />
        </ModalPicker>
        </View>
      </View>
    );
};


const styles = {
  inputWithoutLabel: {
    flex: 1,
    height: 40,
    fontSize: 16,
    padding: 5,
    color: StyleVars.Colors.blueFont,
    lineHeight: 23
  },
  datePicker: {
    paddingRight: 5,
    paddingLeft: 5,
    flex: 2
  },
  inputWithLabel: {
    color: StyleVars.Colors.blueFont,
    paddingRight: 5,
    paddingLeft: 5,
    fontSize: 16,
    lineHeight: 23,
    flex: 2,
    fontWeight: '300'
  },
  labelStyle: {
    fontSize: 12,
    paddingLeft: 20,
    flex: 1,
    fontWeight: '100'
  },
  containerStyle: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center'
  },
  inputWrapperWithoutLabel: {
    borderBottomWidth: 1,
    flex: 1,
    borderColor: StyleVars.Colors.lightLine,
    paddingLeft: 10
  },
  inputWrapperWithLabel: {
    flex: 3,
    borderLeftWidth: 1,
    borderColor: StyleVars.Colors.lightLine,
    paddingLeft: 10
  }
};

export { PickerInput };
