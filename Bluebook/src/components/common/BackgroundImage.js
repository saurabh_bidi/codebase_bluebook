import React from 'react';
import { Image } from 'react-native';


const BackgroundImage = (props) => {
  const { source, children, style } = props;
  return (
    <Image
           source={source}
           style={{ flex: 1, width: null, height: null, ...style }}
           {...props}
    >
    {children}
    </Image>
  );
};

export { BackgroundImage };
