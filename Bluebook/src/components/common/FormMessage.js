import React from 'react';
import { StyleSheet,
         Text } from 'react-native';
import messages from './../meta/messages.json';

const FormMessage = ({ message, type }) => {
  const { error, regular, small } = styles;
  let styleToApply = [error, regular];
  switch (type) {
    case 'error':
      styleToApply = [error, regular];
      break;
    case 'error-small':
      styleToApply = [error, small];
      break;
    default:
      styleToApply = [error, regular];
  }

  console.log(messages);
  console.log(messages[message]);
  return (
    <Text style={styleToApply}>{messages[message]}</Text>
  );
};

const styles = StyleSheet.create({
    error: {
      color: 'red',
      fontWeight: '400',
      padding: 5,
      marginLeft: 5,
      justifyContent: 'center'
    },
    regular: {
      fontSize: 16
    },
    small: {
      fontSize: 12
    }
});

export { FormMessage };
