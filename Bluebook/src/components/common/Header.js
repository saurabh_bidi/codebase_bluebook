import React from 'react';
import { Text,
         View,
         Image,
         TouchableOpacity } from 'react-native';
import LeftButton from './../../images/back.png';
import StyleVars from './../../StyleVars';

// Make a component
const Header = (props) => {
  const { textStyle, viewStyle, centerButtonStyle,
          rightViewStyle, headingStyle, leftButtonImageStyle,
          leftViewStyle, rightButtonStyle } = styles;
  const { hasLeftButton, onLeftPress, leftText, rightText, onRightPress, LeftIcon, rightIcon } = props;

  const renderLeftButton = () => {
    return (
      <TouchableOpacity onPress={onLeftPress}>
        {(leftText) ?
        <Text style={ textStyle }>
          { leftText }
        </Text>
        :
        <Image source={LeftIcon} style={leftButtonImageStyle} />
        }
      </TouchableOpacity>
   );
  };

  return (
    <View style={viewStyle}>
      <View style={leftViewStyle}>
        {(hasLeftButton || leftText) &&
          renderLeftButton()
        }
      </View>
      <View style={headingStyle}>
        <Text style={[textStyle, centerButtonStyle]}>{props.headerText}</Text>
      </View>
      <View style={rightViewStyle}>
        <TouchableOpacity onPress={onRightPress}>
          { (rightText) ?
          <Text style={rightButtonStyle}>
            { rightText }
          </Text>
          :
          <Image source={rightIcon} style={leftButtonImageStyle} />
          }
        </TouchableOpacity>
        
        
      </View>
    </View>
  );
};

const styles = {
  viewStyle: {
    backgroundColor: '#2196F3',//StyleVars.Colors.mediumBackground,
    alignItems: 'center',
    height: 45, //60,
    // paddingTop: 15,
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.2,
    elevation: 2,
    flexDirection: 'row',
    // borderBottomWidth: 1,
    // borderBottomColor: StyleVars.Colors.darkLine
  },
  centerButtonStyle: {
    color: '#fff'
  },
  rightViewStyle: {
    flex: 2, //1
    justifyContent: 'flex-end',
    marginRight: 5
  },
  rightButtonStyle: {
    textAlign: 'center',
    fontSize: 16,
    color: '#fff',
   
    borderRadius: 5
  },
  headingStyle: {
    flex: 5,
    justifyContent: 'center',
    alignItems: 'center'
  },
  textStyle: {
    fontSize: 16,
    color: '#fff',
    textAlign: 'center'
    
  },
  leftButtonImageStyle: {
    height: 16,
    width: 16,
    marginLeft: 5,
    
  },
  leftViewStyle: {
    flex: 2, //1
    marginLeft: 5,
    
    borderRadius: 5
  }
};

// Make the component available to other parts of the app
export { Header };
