import React from 'react';
import { TextInput, View, Text } from 'react-native';
import StyleVars from './../../StyleVars';

const Input = ({ label, value, onChangeText, placeholder, secureTextEntry,
  multiline, numLines, containerHeight, keyboardType }) => {
  const { labelStyle,
          containerStyle,
          inputWrapperWithLabel,
          inputWrapperWithoutLabel,
          inputWithLabel,
          inputWithoutLabel, } = styles;

    const inputStyle = label ? inputWithLabel : inputWithoutLabel;
    const inputWrapperStyleToApply = label ? inputWrapperWithLabel : inputWrapperWithoutLabel;
    const height = containerHeight || 40;
    return (
      <View style={[containerStyle, { height }]}>
      { label !== undefined &&
        <Text style={[labelStyle]}>{label}</Text>
      }
        <View style={inputWrapperStyleToApply}>
            <TextInput
              secureTextEntry={secureTextEntry}
              placeholder={placeholder}
              multiline={multiline}
              numLines={numLines}
              autoCorrect={false}
              value={value}
              style={inputStyle}
              onChangeText={onChangeText}
              keyboardType = {keyboardType}
            />
        </View>
      </View>
    );
};


const styles = {
  inputWithoutLabel: {
    flex: 1,
    height: 40,
    fontSize: 16,
    padding: 5,
    color: StyleVars.Colors.blueFont,
    lineHeight: 23
  },
  datePicker: {
    paddingRight: 5,
    paddingLeft: 5,
    flex: 2
  },
  inputWithLabel: {
    color: StyleVars.Colors.blueFont,
    borderBottomWidth: 1,
    paddingRight: 5,
    paddingLeft: 5,
    fontSize: 16,
    lineHeight: 23,
    flex: 2,
    fontWeight: '300'
  },
  labelStyle: {
    fontSize: 12,
    paddingLeft: 20,
    flex: 1,
    fontWeight: '100'
  },
  containerStyle: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    // borderBottomWidth: 1,
    // borderBottomColor: StyleVars.Colors.lightLine
  },
  inputWrapperWithoutLabel: {
    flex: 1,
    paddingLeft: 10
  },
  inputWrapperWithLabel: {
    flex: 3,
    paddingLeft: 10
  }
};

export { Input };
