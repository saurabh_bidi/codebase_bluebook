import React from 'react';
import { StyleSheet,
         View,
         Dimensions,
         Text } from 'react-native';
import messages from './../meta/messages.json';

const width = Dimensions.get('window').width; //full width
const height = Dimensions.get('window').height; //full height

const ErrorPage = ({ message }) => {
  const { containerStyle, error } = styles;

  return (
    <View style={containerStyle}>
      <Text style={error}>{messages[message]}</Text>
    </View>
  );
};

const styles = StyleSheet.create({
    containerStyle: {
      width,
      height
    },
    error: {
      color: 'red',
      fontWeight: '400',
      padding: 5,
      marginLeft: 5,
      justifyContent: 'center'
    },
    regular: {
      fontSize: 16
    }
});

export { ErrorPage };
