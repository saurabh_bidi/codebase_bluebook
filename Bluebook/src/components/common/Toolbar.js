import React from 'react';
import { Image,
        TouchableOpacity,
        Text,
        View } from 'react-native';
import StyleVars from './../../StyleVars';

const Toolbar = (props) => {
  const { icons, style } = props;
  return (
    <View style={[styles.container, style]}>
      {
        icons.map((item, index) => {
          return (
            <TouchableOpacity key={index} onPress={this.handlePress} style={styles.item} >
                  <Image source={item.source} style={styles.icon} />
                  <Text style={styles.text}>{item.text}</Text>
            </TouchableOpacity>
          );
        })
      }
    </View>
  );
};

const styles = {
  container: {
    marginTop: 3,
    marginBottom: 3,
    flexDirection: 'row',
    justifyContent: 'center'
  },
  icon: {
    alignItems: 'center',
    width: 24,
    height: 24,
    borderRadius: 8,
    backgroundColor: StyleVars.Colors.mediumBackground
  },
  item: {
    alignItems: 'center',
    marginLeft: 15,
    marginRight: 15
  },
  text: {
    fontSize: 10,
    fontWeight: '100',
    color: StyleVars.Colors.blueFont
  }
};

export { Toolbar };
