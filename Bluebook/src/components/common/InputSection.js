import React from 'react';
import { View } from 'react-native';

const InputSection = (props) => {
  return (
    <View style={[styles.sectionStyle, props.style]}>
      {props.children}
    </View>
  );
};

const styles = {
  sectionStyle: {
    borderBottomWidth: 0,
    padding: 3,
    backgroundColor: '#fff',
    justifyContent: 'flex-start',
    flexDirection: 'row',
    borderColor: '#ddd',
    position: 'relative'
  }
};

export { InputSection };
