import React from 'react';
import { Text, TouchableOpacity } from 'react-native';
import StyleVars from './../../StyleVars';

const Button = ({ onPress, children, style,  }) => {
  const { defaultStyle,
          textStyle,
         
          buttonStyle,
          primaryStyle,
          primaryText,

          defaultText } = styles;
  let buttonStyleToApply = defaultStyle;

  let textStyleToApply = defaultText;

  switch (style) {
    case 'primary-default':
      buttonStyleToApply = primaryStyle;
      textStyleToApply = primaryText;
      break;
    default:
      buttonStyleToApply = defaultStyle;
      textStyleToApply = defaultText;
  }
  return (
    <TouchableOpacity onPress={onPress} style={[buttonStyle, buttonStyleToApply]}>
      <Text style={[textStyle, textStyleToApply]}>
        {children}
      </Text>
    </TouchableOpacity>
  );
};

const styles = {
  buttonStyle: {
    flex: 1,
    alignSelf: 'stretch',
    borderRadius: 5,
    borderWidth: 1,
    marginLeft: 5,
    marginRight: 5,
    backgroundColor: 'red',
    borderColor: StyleVars.Colors.mediumBackground
  },
  
  defaultStyle: {
    backgroundColor: '#fff',
    borderColor: '#007aff'
  },
  primaryStyle: {
    backgroundColor: StyleVars.Colors.yellowButton,
    borderColor: StyleVars.Colors.mediumBackground
  },
  defaultText: {
    color: '#007aff',
    fontSize: 16,
    fontWeight: '600'
  },
  primaryText: {
    color: 'black',
    fontSize: 16,
    fontWeight: '600'
  },
  textStyle: {
    alignSelf: 'center',
    paddingTop: 10,
    paddingBottom: 10
  }
};

export { Button };
