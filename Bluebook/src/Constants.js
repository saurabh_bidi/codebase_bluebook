export default {
    Session: {
      user: '',
      base64Image: 'data:image/png;base64,',
      isConnected: false
    },
    PageComponent: {
    	reload: false,
    	alert: false
    }
};
